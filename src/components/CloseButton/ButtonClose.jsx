import React, { Component } from 'react';
import './closeButton.scss'
class ButtonClose extends Component {

    render() {
        const { action } = this.props
        return (

            <>
                <div className='close-btn' onClick={action}>

                </div>
            </>
        )
    }

}
export default ButtonClose;
