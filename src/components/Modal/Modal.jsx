import React, { Component } from 'react';
import ButtonClose from '../CloseButton/ButtonClose';
import Button from '../Button/Button';
import './Modal.scss'

class Modal extends Component {

    render() {

        const { header, text, action, className, headClass } = this.props;

        return (
            <>
                <div onClick={action} className='total-wrapp'>
                    <div className={className} onClick={(e) => {
                        e.stopPropagation()
                    }}>
                        <div className={headClass}>
                            <h1 className='head-title'>{header}</h1>
                            <ButtonClose action={action} />
                        </div>
                        <div className="main-wrapper">
                            {text}
                            <div className="controllers-wrapper">
                                <Button background='controllers ok' textBtn='ok'
                                    onClick={action} />
                                <Button background='controllers cancel' textBtn='Cancel' onClick={action} />

                            </div>
                        </div>

                    </div>
                </div>
            </>
        )

    }
}

export default Modal;
