import React, { Component } from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

class App extends Component {

  state = {
    isOpenedFirst: false,
    isOpenedSecond: false,
    code: 0
  }



  openFirstModal = () => {

    this.setState({ isOpenedFirst: true })

  }
  openSecondModal = () => {

    this.setState({ isOpenedSecond: true })

  }
  closeModalOne = () => {
    this.setState({ isOpenedFirst: false })
  }
  closeModalTwo = () => {
    this.setState({ isOpenedSecond: false })
  }


  render() {

    const { isOpenedFirst, isOpenedSecond } = this.state
    return (
      <>

        <div className="btn-wrapper">
          <Button background={'btn btn-one'} textBtn='First modal' onClick={this.openFirstModal} />
          <Button background={'btn btn-two'} textBtn='Second modal' onClick={() => {
            this.openSecondModal();


          }} />
        </div>

        {isOpenedFirst && <Modal header='Do you want to delete this file?' className='modal-wrapper first-modal' headClass='header-modal-first'
          text="Once you delete this file,it will not be possible to undo this action.Are you sure you want to delete it?"
          action={this.closeModalOne}
        />}

        {isOpenedSecond && <Modal header='Do you want to continue?'
          className='modal-wrapper second-modal' headClass='header-modal-second'
          text="Let it be"
          action={this.closeModalTwo}
        />}
      </>

    )

  }

}

export default App;




